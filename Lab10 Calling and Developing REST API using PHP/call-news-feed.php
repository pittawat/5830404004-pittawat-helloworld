<?php
//Author Pittawat Phongboribun ID: 583040400-4
//This program is load the xml document grom http://www.bangkokbiznews.com/rss/feed/technology.xml
//to show data that teacher define.
header('Content-Type: text/xml; charset=utf-8', true);

$url = 'http://www.bangkokbiznews.com/rss/feed/technology.xml';
$xml = simplexml_load_file($url) or die("Error: Cannot create object");
$length = $xml->channel->item->count();

	echo "<news>";
	echo "<channel>" . $xml->channel->title . "</channel>";
	for ($i=0 ; $i < $length ; $i++ ) {
		echo "<item>";
		echo "<title>" . $xml->channel->item[$i]->title . "</title>";
		echo "<url>" . $xml->channel->item[$i]->link . "</url>";
		echo "</item>";

    }
	echo "</news>";

?>