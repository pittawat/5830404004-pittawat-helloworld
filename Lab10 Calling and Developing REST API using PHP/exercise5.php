<?php
//Author Pittawat Phongboribun ID: 583040400-4
header('Content-Type: text/xml; charset=utf-8', true);
$fileName = 'nations.xml';

// create an object of XMLWriter
$writer = new XMLWriter();
//lets store our XML into the memory so we can output it later
$writer->openMemory();
//lets also set the indent so its a very clean and formatted XML
$writer->setIndent(true);
//now we need to define our Indent string,which is basically how many blank spaces we want to have for the indent
$writer->setIndentString(" ");
//Lets start our document,setting the version of the XML and also the encoding we gonna use
//XMLWriter->startDocument($string version, $string encoding);
$writer->startDocument("1.0", "UTF-8");
// write start tag for element 'nations'
$writer->startElement('nations');

$writer->startElement('nation');
// write element and assign value
$writer->writeElement('name', 'Thailand');
$writer->writeElement('location', 'Southeast Asia');
$writer->endElement(); // end element nation

$writer->startElement('nation');
// write element and assign value
$writer->writeElement('name', 'USA');
$writer->writeElement('location', 'North America');
$writer->endElement(); // end element nation

$writer->endElement(); // end element nation
$writer->endDocument(); // end document nations

echo $writer->flush(true);

?>