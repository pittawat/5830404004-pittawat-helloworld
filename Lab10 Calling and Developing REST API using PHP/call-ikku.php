<?php
//Author Pittawat Phongboribun ID: 583040400-4
//This program is load the xml document grom https://www.kku.ac.th/ikku/api/activities/services/topActivity.php
//to show data that teacher define.
header('Content-Type: application/json; charset=utf-8', true);

$url = 'https://www.kku.ac.th/ikku/api/activities/services/topActivity.php';
$json = file_get_contents($url) or die("Error: Cannot create object");
$node = json_decode($json, true);
$activities = $node['activities'];
$elementCount  = count($activities);
$data = array();

    	for($i=0 ; $i < $elementCount; $i++ ) {
    	 	$dataTitle = $activities[$i]['title'];
    		$dataUrl = $activities[$i]['url'];
    		array_push($data, array('title' => $dataTitle, 'url' => $dataUrl));
    	}

echo json_encode(array("activities" => $data));
?>