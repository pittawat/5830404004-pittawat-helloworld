module.exports = function(app) {

    var facts = require('../controllers/fact.controller.js');

    // Create a new
	app.post('/faculties', facts.create);
	
    // Retrieve all
	app.get('/faculties', facts.findAll);
	
    // Retrieve a single with id
	app.get('/faculties/:id', facts.findOne);
	
    // Update it with id
	app.put('/faculties/:id', facts.update);
	
    // Delete it with id
	app.delete('/faculties/:id', facts.delete);
}