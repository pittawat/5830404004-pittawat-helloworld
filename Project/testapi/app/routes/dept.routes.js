module.exports = function(app) {

    var depts = require('../controllers/dept.controller.js');

    // Create a new
    app.post('/departments', depts.create);
	//app.post('/faculties', facts.create);
	
    // Retrieve all
    app.get('/departments', depts.findAll);
	//app.get('/faculties', facts.findAll);
	
    // Retrieve a single with id
    app.get('/departments/:id', depts.findOne);
	//app.get('/faculties/:id', facts.findOne);
	
    // Update it with id
    app.put('/departments/:id', depts.update);
	//app.put('/faculties/:id', facts.update);
	
    // Delete it with id
    app.delete('/departments/:id', depts.delete);
	//app.delete('/faculties/:id', facts.delete);
}