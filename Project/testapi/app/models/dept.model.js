var mongoose = require('mongoose');

 var deptSchema = new mongoose.Schema({
     id: Number,
	 name: String,
	 desc: String,
	 floor: String,
	 location: {
		 lat: Number,
		 lng: Number
	 }
});

module.exports = mongoose.model('departments', deptSchema);