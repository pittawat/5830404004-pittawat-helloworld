var mongoose = require('mongoose');

 var factSchema = new mongoose.Schema({
     id: Number,
	 name: String,
	 desc: String,
	 location: {
		 lat: Number,
		 lng: Number
	 },
	images: {
		 img1: String,
		 img2: String
	 }
});

module.exports = mongoose.model('faculties', factSchema);