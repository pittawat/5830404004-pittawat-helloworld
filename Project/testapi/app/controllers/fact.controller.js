var Facts = require('../models/fact.model.js');

exports.create = function(req, res) {
	//var id = req.params.id;
    // Create and Save a new Depts
    if(!req.body.id) {
        res.status(400).send({message: "ID can not be empty"});
    }

		var facts = new Facts({id: req.body.id, name: req.body.name, desc: req.body.desc, location: req.body.locaiton, images: req.body.images});
        facts.name = req.body.name;
        facts.desc = req.body.desc;
		facts.floor = req.body.floor;
		facts.location = req.body.location;
    facts.save(function(err, data) {
        console.log(data);
        if(err) {
            console.log(err);
            res.status(500).send({message: "Some error ocuured while creating the faculty."});
        } else {
            res.send(data);
        }
    });
};

exports.findAll = function(req, res) {
    // Retrieve and return all deptss from the database.
    Facts.find(function(err, facts){
        if(err) {
            res.status(500).send({message: "Some error ocuured while retrieving faculty."});
        } else {
            res.send(facts);
        }
    });
};

exports.findOne = function(req, res) {
	var id = req.params.id;
    // Find a single facts with a deptsId
    Facts.findOne({id: id}, function(err, data) {
        if(err || data == null) {
            res.status(500).send({message: "Could not retrieve a faculty with id " + id});
        } else {
            res.send(data);
        }
    });
};

exports.update = function(req, res) {
	var id = req.params.id;
    // Update a facts identified by the faculty id in the request
    Facts.update({id: id}, function(err, facts) {
        if(err) {
            res.status(500).send({message: "Could not find a faculty with id " + id});
        }

		var facts = new Facts({id: req.body.id, name: req.body.name, desc: req.body.desc, floor: req.body.floor, location: req.body.locaiton});
        facts.name = req.body.name;
        facts.desc = req.body.desc;
		facts.floor = req.body.floor;
		facts.location = req.body.location;

         facts.save(function(err, data){
             if(err) {
                 res.status(500).send({message: "Could not update a faculty with id " + id});
             } else {
                 res.send(data);
             }
         });
    });
};

exports.delete = function(req, res) {
	var id = req.params.id;
    // Delete a faculty with the specified id in the request
    Facts.remove({id: id}, function(err, data) {
        if(err || String(data) == "{\"n\":0,\"ok\":1}") {
            res.status(500).send({message: "Could not delete a faculty with id " + id});
        } else {
            res.send({message: "ID : " + id + " has deleted successfully!"})
        }
    });
};