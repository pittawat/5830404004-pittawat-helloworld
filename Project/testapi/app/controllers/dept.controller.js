var Depts = require('../models/dept.model.js');

exports.create = function(req, res) {
	//var id = req.params.id;
    // Create and Save a new Depts
    if(!req.body.id) {
        res.status(400).send({message: "ID can not be empty"});
    }

    //var depts = new Depts({id: req.body.id, name: req.body.name, desc: req.body.desc, floor: req.body.floor, location: req.body.locaiton});
		var depts = new Depts({id: req.body.id, name: req.body.name, desc: req.body.desc, floor: req.body.floor, location: req.body.locaiton});
        depts.name = req.body.name;
        depts.desc = req.body.desc;
		depts.floor = req.body.floor;
		depts.location = req.body.location;
    depts.save(function(err, data) {
        console.log(data);
        if(err) {
            console.log(err);
            res.status(500).send({message: "Some error ocuured while creating the Depts."});
        } else {
            res.send(data);
        }
    });
};

exports.findAll = function(req, res) {
    // Retrieve and return all depts from the database.
    Depts.find(function(err, depts){
        if(err) {
            res.status(500).send({message: "Some error ocuured while retrieving depts."});
        } else {
            res.send(depts);
        }
    });
};

exports.findOne = function(req, res) {
	var id = req.params.id;
    // Find a single depts with a deptsId
    Depts.findOne({id: id}, function(err, data) {
        if(err || data == null) {
            res.status(500).send({message: "Could not retrieve a department with id " + id});
        } else {
            res.send(data);
        }
    });
};

exports.update = function(req, res) {
	var id = req.params.id;
    // Update a depts identified by the deptsId in the request
    Depts.update({id: id}, function(err, depts) {
        if(err) {
            res.status(500).send({message: "Could not find a department with id " + id});
        }

		var depts = new Depts({id: req.body.id, name: req.body.name, desc: req.body.desc, floor: req.body.floor, location: req.body.locaiton});
        depts.name = req.body.name;
        depts.desc = req.body.desc;
		depts.floor = req.body.floor;
		depts.location = req.body.location;

         depts.save(function(err, data){
             if(err) {
                 res.status(500).send({message: "Could not update a department with id " + id});
             } else {
                 res.send(data);
             }
         });
    });
};

exports.delete = function(req, res) {
	var id = req.params.id;
    // Delete a department with the specified id in the request
    Depts.remove({id: id}, function(err, data) {
        if(err || String(data) == "{\"n\":0,\"ok\":1}") {
            res.status(500).send({message: "Could not delete a department with id " + id});
        } else {
            res.send({message: "ID : " + id + " has deleted successfully!"})
        }
    });
};