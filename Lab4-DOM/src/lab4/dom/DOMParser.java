/*
This a program to read XML document name nation.xml
It can get root name and attribute id of root
@Author Pittawat Phongboribun ID 583040400-4
Date 24/08/60
 */
package lab4.dom;

import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;


public class DOMParser {
    public static void main(String argv[]) {
        
    try {

	
	DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	File xmlFile = new File("nation.xml");
        Document doc = dBuilder.parse(xmlFile);
        doc.getDocumentElement().normalize();
        
        System.out.println("The root element name is " + doc.getDocumentElement().getNodeName());
        System.out.println("The root element has attribute " + doc.getDocumentElement().getAttributeNode("id"));

        } catch (Exception e) {
	e.printStackTrace();
    }

    }
}
