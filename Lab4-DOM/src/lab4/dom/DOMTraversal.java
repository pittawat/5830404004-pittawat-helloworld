/*
This a program to travers XML document name nation.xml
It can get node type, name, values and number of child node
@Author Pittawat Phongboribun ID 583040400-4
Date 24/08/60
 */
package lab4.dom;

import java.io.File;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class DOMTraversal {
        public static void main(String argv[]) throws Exception {

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            File xmlFile = new File("nation.xml");
            Document doc = dBuilder.parse(xmlFile);
            Node node = doc.getDocumentElement();
            followNode(node);
        } 

        public static void followNode(Node node) throws IOException {
            String name = node.getNodeName();
            String type = new String();
            String value = node.getNodeValue();
            int numChildren = node.getChildNodes().getLength();
            if (node.hasChildNodes()) {
                if (node.getNodeType() == 1) {
                    type = "element";
                }
                if (node.getNodeType() == 3) {
                    type = "text";
                }
                
                System.out.println("Node:type is " + type + " name is " + name + " values is " + value);
                System.out.println("node "+ name + " has " + numChildren + " children");
                Node firstChild = node.getFirstChild();
                followNode(firstChild);
            } else if (!node.hasChildNodes()){
                    System.out.println("Node:type is " + type + " name is " + name + " values is " + value);
                }
                Node nextNode = node.getNextSibling();
                if (nextNode != null) followNode(nextNode);
        }

}
