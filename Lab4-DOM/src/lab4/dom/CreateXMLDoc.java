/*
This a program to create XML document name myProfile.xml
It have root node name is prifile and attribute id, 3 element
@Author Pittawat Phongboribun ID 583040400-4
Date 24/08/60
 */
package lab4.dom;

import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;


public class CreateXMLDoc {
    
    	public static void main(String[] args) throws Exception {
		 String xmlFileName = "myProfile.xml";
		//Create document
		DocumentBuilderFactory fact = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = fact.newDocumentBuilder();
		Document doc = builder.newDocument();
		//Create root node and child node
		Element root = doc.createElement("profile");
                Element name = doc.createElement("name");
                Element major = doc.createElement("major");
                Element area = doc.createElement("area");
                //Assign values
                Text myName = doc.createTextNode("Pittawat Phongboribun");
                Text majorName = doc.createTextNode("Computer Engineering");
                Text areaName = doc.createTextNode("Software");
                //Append nodes
                name.appendChild(myName);
                major.appendChild(majorName);
                area.appendChild(areaName);
		doc.appendChild(root);
                root.appendChild(name);
                root.appendChild(major);
                major.appendChild(area);
                
		//Set attribute root node
		root.setAttribute("id","5830404004");
                
                //Output to XML file
                Transformer transformer = TransformerFactory.newInstance().newTransformer();
                DOMSource source = new DOMSource(doc);
                StreamResult result = new StreamResult(new File(xmlFileName));
                transformer.transform(source, result);
                System.out.println("file is save now!!");

	}
}
