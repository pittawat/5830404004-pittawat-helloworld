<?php
//Create DOM document and set output file name.
$fileName = "myProfile.xml";
$doc = new DomDocument('1.0');

//Create element.
$root = $doc->createElement('profile');
$majorElement = $doc->createElement('major');
$areaElement = $doc->createElement('area');
$nameElement = $doc->createElement('name');

//Create text node.
$majorValue = $doc->createTextNode("Computer Engineering");

$areaValue = $doc->createTextNode("Software");

$nameValue = $doc->createTextNode("Pittawat Phongboribun");


//Append child and set root's attribute.
$doc->appendChild($root);
$root->setAttribute('id', '5830404004');
$majorElement->appendChild($majorValue);
$areaElement->appendChild($areaValue);
$nameElement->appendChild($nameValue);
$majorElement->appendChild($areaElement);
$root->appendChild($majorElement);
$root->appendChild($nameElement);


//Create xml document.
$doc->save($fileName);
echo "Finish writing file $fileName";
?>
