<?php

$url = "http://www.webservicex.net/globalweather.asmx?WSDL";

	try {
		$client = new SoapClient($url);
		$call = $client->GetCitiesByCountry(array('CountryName' => "Thailand"));
	}
	catch(Exception $e) {
		die($e->getMessage());
	}

$xml = simplexml_load_string($call->GetCitiesByCountryResult);
echo "<h1>List of countries in Thailand by webservicex.com</h1>";
echo "<ul>";
	foreach ($xml->Table AS $result) {
		echo "<li>" . $result->City ."</li>";
	}
	
echo "</ul>";
?>