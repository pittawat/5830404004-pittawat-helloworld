<?php
$url = "http://www.pttplc.com/webservice/pttinfo.asmx?WSDL"; 

	try {
		$client = new SoapClient($url, array("trace" => 1, "exceptions" => 0, "cache_wsdl" => 0)); 
		$call = $client->CurrentOilPrice(array('Language' => "EN"));
	}
	catch(Exception $e) {
		die($e->getMessage());
	}

echo "<h1>Current oil price in Thailand</h1>";
$xml = simplexml_load_string($call->CurrentOilPriceResult);
echo "<table>";
echo "<tr>";
echo "<th>Product</th>";
echo "<th>Price</th>";
echo "</tr>";
    foreach($xml->DataAccess as $result){
        echo "<tr>";
        echo "<td>$result->PRODUCT</td>";
        echo "<td>$result->PRICE</td>";
        echo "</tr>";
	}
echo "</table>";

?>