'use strict';
var mongoose = require('mongoose');
Task = mongoose.model('Tasks');

exports.list_all_tasks = function(require, response){
    Task.find({}, function(error, task) {
        if (error)
            response.send(error);
        response.json(task);
    })
};

exports.create_a_task = function(require, response){
    var new_task = new Task(require.body);
    new_task.save(function(error, task) {
        if (error)
            response.send(error);
        response.json(task);
    })
};

exports.read_a_task = function(require, response){
    Task.findById(require.params.taskId, function(error, task) {
        if (error)
            response.send(error);
        response.json(task);
    })
;}

exports.update_a_task = function(require, response){
    Task.findOneAndUpdate({_id: require.params.taskId}, require.body, {new: true},
        function(error, task) {
            if(error)
                response.send(error);
        response.json(task);
    })
};

exports.delete_a_task = function(require, response){
    Task.remove({
        _id: require.params.taskId
    }, function(error, task){
        if (error)
                response.send(error);
        response.json({message: 'Task successfull deleted'});
    });
}