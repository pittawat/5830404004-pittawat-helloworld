// file system module to perform file operations

var file = 'data.json'
const fs = require('fs');
 
// json data
let jsonData = {
    "name": "CM",
    "courses": [
        "198330",
        "198371"
    ],
    "places": {
        "residence": "Khon Kaen",
        "visits": [
            "Songkla",
            "Bangkok"
        ]
    }
};
 
let data = JSON.stringify(jsonData, null, 2);
fs.writeFileSync(file, data);


var jsonfile = require('jsonfile')
jsonfile.readFile(file, function(err, obj) {
	if (err) throw err
	console.log("=== Output part 1: the whole object from reading " + file + " ===");
	console.dir(obj);
	console.log("=== Output part 2: the value of the second course  and the residence ===");
	console.log("Studying " + obj.courses[1] + " living in " + obj.places.residence);
})